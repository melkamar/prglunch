from typing import *
from dataclasses import dataclass


@dataclass
class MenuItem:
    name: str
    price: int


@dataclass
class Restaurant:
    name: str
    menu: List[MenuItem]
    menu_url: str
