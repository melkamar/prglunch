import json
import logging
import re
from abc import ABC, abstractmethod
from datetime import datetime
from typing import List

import bs4.element
import requests
import time
from bs4 import BeautifulSoup
from selenium import webdriver

from prglunch.model import MenuItem, Restaurant

log = logging.getLogger(__name__)

DAYS_OF_WEEK = [
    'pondělí',
    'úterý',
    'středa',
    'čtvrtek',
    'pátek',
    'sobota',
    'neděle',
]

__all__ = ['scrapers_list']

# This list will contain all scrapers registered by @scraper
scrapers_list = []


def scraper(cls):
    """
    Decorator to register a scraper class. All scraper classes that should be used must be
    decorated using this.

    The decorator just instantiates the class and stores it in the scrapers_list.

    :param cls: The decorated class
    :return:
    """
    scrapers_list.append(cls())

    def wrapped_class():
        return cls()

    return wrapped_class


class BaseScraper(ABC):
    """
    Interface for concrete scrapers.

    A scraper is responsible for fetching and parsing a restaurant menu.
    """
    REGEX_PRICE_PATTERN = r'(?:(\d+)\s*[Kk][Čč])|(?:(\d+)\s*,[-–])'

    def get_restaurant(self):
        return Restaurant(self.name, self.fetch_menu(), self.menu_url)

    @abstractmethod
    def fetch_menu(self) -> List[MenuItem]:
        pass

    @property
    @abstractmethod
    def name(self) -> str:
        pass

    @property
    @abstractmethod
    def menu_url(self) -> str:
        pass

    def get_menu_page(self, override_encoding=False) -> str:
        response = requests.get(self.menu_url)
        response.raise_for_status()

        if override_encoding:
            response.encoding = response.apparent_encoding
        return response.text.replace('\xa0', ' ')

    @staticmethod
    def parse_price_kc(price_kc_text: str) -> int:
        m = re.search(BaseScraper.REGEX_PRICE_PATTERN, price_kc_text)
        if m:
            try:  # Return the first non-None captured group value
                return int(next(x for x in m.groups() if x))
            except StopIteration:
                pass  # Fall to the return below

        return -1

    def get_soup_of_menu(self) -> BeautifulSoup:
        """Get the BeautifulSoup object populated with the downloaded HTML menu"""
        return BeautifulSoup(self.get_menu_page(), 'html.parser')

    @staticmethod
    def get_current_weekday() -> int:
        return datetime.now().weekday()


@scraper
class OliveScraper(BaseScraper):
    def fetch_menu(self) -> List[MenuItem]:
        result = []

        # True if the iteration is inside the day we want the menu for. If this is False then we
        # are not adding menu items to the result set
        inside_wanted_day = False

        soup = self.get_soup_of_menu()
        meal_wrappers_tags = soup.select('div#detail_content_block tr')
        for meal_wrapper in meal_wrappers_tags:
            day_of_week_word = DAYS_OF_WEEK[self.get_current_weekday()]
            if meal_wrapper.text.strip().lower() == day_of_week_word:
                inside_wanted_day = True
                continue

            if not inside_wanted_day:
                continue

            if meal_wrapper.text.strip().lower() in DAYS_OF_WEEK:
                # We are already inside a day of week we are looking for and just got a header for
                # another day of week -> we are done
                break

            # e.g. "Polévka: **Kulajda s houbami / Dill sour soup"
            meal_name = meal_wrapper.select_one('th').text

            # Ignore certain meals
            if meal_name in [
                'Denní nabídka jídel z Hummus a Gyros baru!'
            ]:
                continue

            full_meal_price = meal_wrapper.select_one('td').text
            price_match = re.search(self.REGEX_PRICE_PATTERN, full_meal_price)
            if price_match:
                meal_price = price_match.group(1)
            else:
                meal_price = -1

            result.append(MenuItem(meal_name, meal_price))

        return result

    @property
    def name(self) -> str:
        return 'Olive'

    @property
    def menu_url(self) -> str:
        return 'http://www.olivefood.cz/olive-florentinum/10/'


@scraper
class PotrefenaHusaScraper(BaseScraper):
    def fetch_menu(self) -> List[MenuItem]:
        result = []

        soup = self.get_soup_of_menu()
        meals_div = soup.select_one('div.denninabidka')

        meal_names_tags = meals_div.select('h4')
        meal_prices_tags = meals_div.select('span.price')
        meal_details_tags = meals_div.select('p')

        for meal_name_tag, meal_price_tag, meal_details_tag in zip(meal_names_tags,
                                                                   meal_prices_tags,
                                                                   meal_details_tags):
            meal_full_name = '{} {}'.format(meal_name_tag.text.replace('\xa0', ' ').strip(),
                                            meal_details_tag.text.replace('\xa0', ' ').strip())

            meal_price = self.parse_price_kc(meal_price_tag.text.strip())

            result.append(MenuItem(meal_full_name, meal_price))

        return result

    @property
    def name(self) -> str:
        return 'Potrefená Husa'

    @property
    def menu_url(self) -> str:
        return 'https://www.potrefena-husa.eu/'


@scraper
class Restaurant2002Scraper(BaseScraper):
    def fetch_menu(self) -> List[MenuItem]:
        result = []

        soup = self.get_soup_of_menu()
        meals_items = soup.select('div.dailyMenu__item')

        for meal_item in meals_items:
            name = meal_item.select_one('div.dailyMenu__meal span.dailyMenu__meal-name').text
            price = meal_item.select_one('div.dailyMenu__meal span.dailyMenu__meal-price')
            weight = meal_item.select_one('div.dailyMenu__meal-weight').text

            if 'dez.' in weight:
                # skip desserts
                continue

            if '100g' in weight:
                # skip salads (other meals have more info than just grams)
                continue

            name_sanitized = re.sub(r'\([^)]*\)', '', name)
            price_sanitized = self.parse_price_kc(price.text.strip())

            if price_sanitized < 30:
                # skip too cheap items (probably stuff like tea, drinks)
                continue

            result.append(MenuItem(name_sanitized, price_sanitized))

        return result

    @property
    def name(self) -> str:
        return '2002'

    @property
    def menu_url(self) -> str:
        return 'https://www.dvatisicedva.cz/'


@scraper
class MasaryckaScraper(BaseScraper):
    def fetch_menu(self) -> List[MenuItem]:
        soup = self.get_soup_of_menu()

        json_data = json.loads(soup.select_one('script#__NEXT_DATA__').text)

        days_ids = {
            cat['hurl']: cat['_id'] for cat in
            json_data['props']['app']['categories']
        }

        today_day_number = datetime.now().weekday()
        if today_day_number in range(5):
            day_slug = [
                'pondeli',
                'utery',
                'streda',
                'ctvrtek',
                'patek',
            ][today_day_number]
        else:
            day_slug = 'pondeli'

        day_id = days_ids[day_slug]

        meals_json = [
            meal for meal in json_data['props']['app']['menu']
            if meal['category'] == day_id
        ]

        return [
            MenuItem(meal['name'], int(meal['price'] / 100))
            for meal in meals_json
        ]

    @property
    def name(self) -> str:
        return 'Masaryčka'

    @property
    def menu_url(self) -> str:
        return 'https://masaryckarestaurace.choiceqr.com/61b8a551295b45a92dc6dcde/61b8a55f295b456a05c6dce4'


@scraper
class DhabaScraper(BaseScraper):
    def fetch_menu(self) -> List[MenuItem]:
        return [
            MenuItem("Bufet", 0)
        ]

    @property
    def name(self) -> str:
        return 'Dhaba Beas'

    @property
    def menu_url(self) -> str:
        return 'https://www.potrefena-husa.eu/'


@scraper
class AlKarimScraper(BaseScraper):
    def fetch_menu(self) -> List[MenuItem]:
        return [
            MenuItem("100 g", 25)
        ]

    @property
    def name(self) -> str:
        return 'Al Karim indický bufet'

    @property
    def menu_url(self) -> str:
        return 'https://www.potrefena-husa.eu/'


@scraper
class BahnMiScraper(BaseScraper):
    def fetch_menu(self) -> List[MenuItem]:
        return [
            MenuItem("TODO", -1)
        ]

    @property
    def name(self) -> str:
        return 'Bahn-mi-ba'

    @property
    def menu_url(self) -> str:
        return 'https://www.potrefena-husa.eu/'


@scraper
class SiaScraper(BaseScraper):
    def fetch_menu(self) -> List[MenuItem]:
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        driver = webdriver.Chrome(options=options)
        driver.implicitly_wait(5)
        driver.get(self.menu_url)

        time.sleep(2)

        result = []
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        day_meals = soup.select_one(f'div#dm-week-{datetime.now().weekday()}')

        categories = day_meals.select('div.dm-cat')
        for category in categories:
            category_title = category.select_one('div.dm-cat-title').text.strip()

            item = category.select_one('div.dm-item div.dm-content')
            if not item:
                continue

            name = item.select_one('h3').text
            price = self.parse_price_kc(item.select_one('strong.cen').text)

            result.append(
                MenuItem(
                    f'{name} ({category_title})',
                    price
                )
            )

        return result

    @property
    def name(self) -> str:
        return 'Sia'

    @property
    def menu_url(self) -> str:
        return 'https://www.siarestaurant.cz/tydenni-menu/'


@scraper
class LaRepublicaScraper(BaseScraper):
    def fetch_menu(self) -> List[MenuItem]:
        soup = self.get_soup_of_menu()

        div = soup.select_one('div.blok')

        ps = div.select('p')

        day_of_week_word = DAYS_OF_WEEK[self.get_current_weekday()]
        meals = []

        collect_next = False
        for p in ps:
            if not collect_next:
                if p.text.lower().startswith(day_of_week_word):
                    collect_next = True
                continue

            if collect_next:
                for item in p.contents:
                    if type(item) is bs4.element.NavigableString:
                        name, price_txt = item.text.rsplit(' ', 1)
                        price_parsed = self.parse_price_kc(price_txt)
                        meals.append(MenuItem(name, price_parsed))

                break

        return meals

    @property
    def name(self) -> str:
        return 'La Republica'

    @property
    def menu_url(self) -> str:
        return 'https://www.larepublica.cz/cz/poledni-menu/'
