import argparse
import logging

from prglunch import slack
from prglunch.model import *
from prglunch.scrapers import scrapers_list

logging.basicConfig(level=logging.DEBUG)
logging.getLogger("urllib3").setLevel(logging.WARNING)
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("chardet").setLevel(logging.WARNING)


def get_restaurants() -> List[Restaurant]:
    result = []
    for sc in scrapers_list:
        try:
            result.append(sc.get_restaurant())
        except Exception as e:
            logging.exception(e)
            result.append(
                Restaurant(sc.name, [MenuItem('Failed to parse', -1)], sc.menu_url)
            )
    return result


def restaurant_to_slack_poll(restaurant: Restaurant) -> str:
    meals = "\n".join(f'- {m.name} *({m.price},-)*' for m in restaurant.menu)
    return f'*{restaurant.name}*\n{meals}\n\n'


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('SLACK_TOKEN')
    parser.add_argument('SLACK_CHANNEL')
    return parser.parse_args()


def main():
    # s = SiaScraper()
    # m = s.fetch_menu()
    # print(m)

    args = parse_args()

    restaurants = get_restaurants()
    slack.post_poll(restaurants, args.SLACK_TOKEN, args.SLACK_CHANNEL)


if __name__ == '__main__':
    main()
