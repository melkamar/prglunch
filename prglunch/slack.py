import logging
from typing import *
import time

import requests
from prglunch import model

logger = logging.getLogger(__name__)

emoji_lookup = {
    "1": ":one:",
    "2": ":two:",
    "3": ":three:",
    "4": ":four:",
    "5": ":five:",
    "6": ":six:",
    "7": ":seven:",
    "8": ":eight:",
    "9": ":nine:",
    "0": ":zero:",
}


def _index_into_emoji(index: int) -> str:
    result = []
    for char in str(index):
        result.append(emoji_lookup[char])
    return " ".join(result)


def _restaurant_into_text(restaurant: model.Restaurant, index: int) -> str:
    menu_list_str = '\n'.join([
        f'- {menuitem.name} ({menuitem.price},-)' for menuitem in restaurant.menu
    ])

    return f'''{_index_into_emoji(index)} - *{restaurant.name}*
{menu_list_str}
'''


def _restaurants_as_slack_blocks(restaurants: List[model.Restaurant]) -> List:
    result = []

    for index, restaurant in enumerate(restaurants):
        result.append(
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": _restaurant_into_text(restaurant, index + 1)
                },
                "accessory": {
                    "type": "button",
                    "text": {
                        "type": "plain_text",
                        "text": "Website",
                        "emoji": True
                    },
                    "value": "click_me_123",
                    "url": restaurant.menu_url,
                    "action_id": "button-action"
                }
            }
        )

    return result


def _build_slack_message(restaurants: List[model.Restaurant]) -> Dict:
    divider = {
        "type": "divider"
    }

    restaurant_blocks = _restaurants_as_slack_blocks(restaurants)
    restaurant_blocks_with_dividers = []
    for block in restaurant_blocks:
        restaurant_blocks_with_dividers.append(block)
        restaurant_blocks_with_dividers.append(divider)

    return {
        "attachments": [
            {
                "blocks": [
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": ":pizza: :shallow_pan_of_food: :corn: :tomato: :hamburger: :cut_of_meat: :poultry_leg: :spaghetti: :ramen: :sushi: :bento: :stew: :hotdog: :taco: :cheese_wedge: :fries: :fried_shrimp: :burrito: :sandwich: \n*Hungry? Vote on where to go with emojis!*"
                        }
                    },
                    divider,
                    *restaurant_blocks_with_dividers
                ]
            }
        ]
    }


def post_poll(restaurants: List[model.Restaurant], slack_token: str, slack_channel: str):
    data = _build_slack_message(restaurants)
    logger.debug(data)

    data['channel'] = slack_channel

    result = requests.post(
        'https://slack.com/api/chat.postMessage',
        json=data,
        headers={
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization': f'Bearer {slack_token}'
        }
    )

    result.raise_for_status()
    js = result.json()
    if not js['ok']:
        raise ValueError(js)

    ts = js['ts']
    # add preemptive emoji reactions
    for i, _ in enumerate(restaurants):
        result = requests.post(
            'https://slack.com/api/reactions.add',
            json={
                'channel': slack_channel,
                'timestamp': ts,
                'name': _index_into_emoji(i + 1).replace(':', '')
            },
            headers={
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': f'Bearer {slack_token}'
            }
        )
        time.sleep(1)
        result.raise_for_status()
        js = result.json()
        if not js['ok']:
            raise ValueError(js)
